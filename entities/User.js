class User {

    constructor( name ) { 
        this.name = name; 
        this.startTime = Math.floor(Math.random()*300); // random second between 0 - 300 (between 0 - 5 minutes)
        this.items = Math.floor((Math.random()*999999))+1; // random second between 1 - 1000000
        this.processItems = 0;
    }

    getTimeCome(){
        return `${String(parseInt(this.startTime/60)).padStart(2, '0')}:${String(parseInt(this.startTime%60)).padStart(2, '0')}`;
    }
  
  }

  export default User;