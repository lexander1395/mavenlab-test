class dataueue {

    constructor() { 
        this.data = [];
        this.currentIndex=-1; 
        this.currentUser=null;
    }
    come( user ) { 
        this.data.push(  user );
        if(this.currentIndex==-1){
            this.currentIndex = 0;
            this.currentUser = this.data[0];
        } 
    }
    next() { 
        if(this.currentIndex+1 >= this.data.length){
            this.currentIndex = 0;
        }else{
            this.currentIndex++;
        }
        this.currentUser= this.data[this.currentIndex];
    }
    isEmpty() { return this.data.length == 0; }
    removeUser(index) { 
        this.data.splice(index, 1); 
     }
    print() { console.log(this.data) }
  
  }

  export default dataueue;