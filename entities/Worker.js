import {
    parentPort,
    workerData
  } from "worker_threads";

const duration = Math.floor((Math.random()*4))+1; //generate number between 1 - 5

setTimeout(()=>parentPort.postMessage({ processItems:workerData.processItems,start:workerData.start }),duration*1000);
