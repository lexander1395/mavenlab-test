import { Worker } from 'worker_threads';
import Queue from './Queue.js';
import User from './User.js';

class Timer {
    static seconds = 0;
    static users = [];
    static totalItems = 0;
    static q = new Queue();
    static currentUserIndex = 0;
    static availWorkers = [true,true];

    static init(){
        this.users = [
            new User('A'),
            new User('B'),
            new User('C'),
            new User('D'),
            new User('E'),
            new User('F'),
            new User('G'),
            new User('H'),
            new User('I'),
            new User('J')
        ];
        this.users.sort((a,b)=>a.startTime - b.startTime);
        console.log(`Generated user inputs:`);
        this.users.forEach(user=>{
            this.totalItems+=user.items;
            console.log(`User ${user.name}: ${user.items} items - scheduled at ${user.getTimeCome()}`);
        });
    }

    static start(){
        this.run = true;
        const intervalId = setInterval(()=>{
            if(this.totalItems > 0) {
                this.checkUserCome();
                if(!this.q.isEmpty() && this.isAvailWorkers() !== 0){
                    const totalAvail = this.isAvailWorkers();
                    for (let index = 0; index < totalAvail; index++) {
                        const availWorker = this.getAvailWorkers();
                        this.processItem(this.q.currentIndex,this.q.currentUser,availWorker);
                        this.nextQ();
                    }
                }else{
                    if(this.q.isEmpty()){
                        console.log(`${this.getTime()} - Waiting Queue`);
                    }
                }
                this.seconds++;
           } else {
                if(this.isAvailWorkers() == 2){
                    clearInterval(intervalId);
                    console.log("Engine has processed all data.");
                }
                this.seconds++;
           }
        }, 1000);
    }

    static checkUserCome(){
        this.users.forEach(user=>{
            if(this.seconds == user.startTime){
                this.q.come(user);
                console.log(`${this.getTime()} - User ${user.name} comes in to process ${user.items} items`);
            }
        });
    }

    static getTime(){
        return `${String(parseInt(this.seconds/60)).padStart(2, '0')}:${String(parseInt(this.seconds%60)).padStart(2, '0')}`;
    }

    static async processItem(index,user,availWorker) {
        if(availWorker > 0 && user.processItems < user.items){
            this.availWorkers[availWorker-1] = false;
            this.q.data[index].processItems++;
            console.log(`${this.getTime()} - Thread ${availWorker} - User ${user.name}: start processing item ${user.processItems}`);
            const worker = new Worker('./entities/Worker.js', { workerData:{...user,start:this.seconds} });
            worker.on('message', (msg)=>{
                this.nextQ();
                this.totalItems--;
                this.availWorkers[availWorker-1] = true;
                const duration = this.seconds - msg.start;
                console.log(`${this.getTime()} - Thread ${availWorker} - User ${user.name}: finished processing item ${msg.processItems} (Duration: ${duration}s)`);
            });
        }
    }

    static getAvailWorkers(){
        for (let index = 0; index < this.availWorkers.length; index++) {
            const worker = this.availWorkers[index];
            if(worker){
                return index+1;
            }
        }
        return -1;
    }

    static isAvailWorkers(){
        let avail = 0;
        for (let index = 0; index < this.availWorkers.length; index++) {
            const worker = this.availWorkers[index];
            if(worker){
                avail++;
            }
        }
        return avail;
    }

    static nextQ(){
        const fromIndex = this.q.currentIndex;
        do{
            this.q.next();
        }
        while(fromIndex != this.q.currentIndex && this.q.currentUser.processItems == this.q.currentUser.items);
    }

}

export default Timer;