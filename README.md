# maven lab coding test alexander gunawan

## how i approach the solution for the problem

first i make Classes that i think i will needed for this test, which i created in folder ./entities :
+Queue.js //for handling user queue management
+Timer.js //for handling timer event which fire every second as a fondation of this apps
+User.js //for handling user object which contain when the user start to come, how many task that user have, and how many items that has been process for each user
+Worker.js //for handling simulation of making the process which in this case i use setTimeout function with random number between 1-5

and for threading i use worker_thread library from nodejs

## how the program works 

i start simulation of the timer using setInterval

which for each interval the program will check wether is there any user come at that time or not,
after that the program will also check is there any worker available to process available task in user queue. ( the processing task will use worker thread from nodejs)
for each available workers the program will put user task to maximize worker performance.
if the task that been given to the worker is done then the worker will become available again and be ready to accept next task.

on each interval if there is no available task from all user then the program will end.

## how to run the program

if you are already have nodejs installed on your machine, you can run the program using :

npm start

if you dont have nodejs but you have docker on your machine then you can run via docker using these commands :

docker build . -t mavenlab-test

docker run --name mavenlab -d mavenlab-test

docker logs -f mavenlab